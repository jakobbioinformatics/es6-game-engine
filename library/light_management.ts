/// <reference path="game_objects.ts" />
/// <reference path="game_utilities.ts" />

var LIGHT_RADIUS = 100;

class LightManager {

    BASE_OPACITY: number = 0.5;

    pixi_stage:PIXI.Container;
    gc:GameContent;
    dark_matrix: DarkSquare[][];
    dark_list: DarkSquare[];
    light_sources: GameObject[];

    constructor(pixi_stage:PIXI.Container, gc:GameContent) {

        this.pixi_stage = pixi_stage;
        this.gc = gc;

        this.dark_matrix = [];
        this.dark_list = [];
        this.light_sources = undefined;
    }

    assign_light_sources(game_objects:GameObject[]) {
        this.light_sources = [];
        //console.log('looking for lightsources...');
        //console.log(`Objects: ${game_objects.length}`);
        for (var i = 0; i < game_objects.length; i++) {
            //console.log(game_objects[i].label);
            var obj = game_objects[i];
            //console.log(obj.is_lightsource);
            if (obj.is_lightsource)  {
                this.light_sources.push(obj);
                //console.log('found lightsource!');
            }
        }
    }

    generate_darkness(game_state:State) {

        this.dark_list = [];
        this.dark_matrix = [];

        for (var x = 0; x < this.gc.xtiles; x++) {
            var col = [];
            for (var y = 0; y < this.gc.xtiles; y++) {
                var sq = new DarkSquare(x * this.gc.tile_size, y * this.gc.tile_size,
                                        this.BASE_OPACITY, this.gc);
                col.push(sq);
                this.dark_list.push(sq);
            }
            this.dark_matrix.push(col);
        }

        for (var i = 0; i < this.dark_list.length; i++) {
            game_state.add_overlay_object(this.dark_list[i]);
        }
    }

    update() {
        for (var x = 0; x < this.gc.xtiles; x++) {
            for (var y = 0; y < this.gc.xtiles; y++) {
                this.dark_matrix[x][y].update(this.light_sources);
            }
        }
    }

    get_light_sources() {
        return this.light_sources;
    }
}

class DarkSquare extends GameObject {

    base_alpha: number;

    constructor(x:number, y:number, base_alpha:number, gc:GameContent) {
        var sprite_rect = new PIXI.Rectangle(32, 16, gc.tile_size, gc.tile_size);
        super(gc.tileset16sheet, sprite_rect, x, y);

        this.base_alpha = base_alpha;
        this.set_alpha(this.base_alpha);
        //overlay_container.addChild(this.get_sprite());
        //pixi_stage.addChild(this.get_sprite());
    }

    update(light_sources) {

        if (light_sources.length > 0) {

            var closest_source = this.get_closest_object(light_sources);
            var closest_dist = this.get_dist_to_object(closest_source);

            if (closest_dist < LIGHT_RADIUS) {
                this.set_alpha(this.base_alpha * (closest_dist / LIGHT_RADIUS));
            }
        }
    }

    _get_closest_distance(light_sources) {

        var closest_dist = 1000;
        for (var i = 0;  i < light_sources.length; i++) {
            var source = light_sources[i];
            var distance = this._get_distance(source.x, source.y);
            if (distance < closest_dist) {
                closest_dist = distance;
            }
        }
        return closest_dist;
    }

    _get_distance(other_x, other_y) {

        return Math.sqrt(Math.pow(this.get_x() - other_x, 2) + Math.pow(this.get_y() - other_y, 2));
    }
}
