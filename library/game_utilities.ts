/// <reference path="main_logic.ts" />

class GameContent {

    level_width:number;
    level_height:number;
    tile_size:number;
    xtiles:number;
    ytiles:number;
    total_tiles:number;
    total_levels:number;

    background:PIXI.Texture;
    tilesheet:PIXI.Texture;
    enemysheet:PIXI.Texture;
    firesheet:PIXI.Texture;
    playersheet:PIXI.Texture;
    tileset16sheet:PIXI.Texture;

    constructor() {
        this.level_width = 800;
        this.level_height = 480;
        this.tile_size = 16;
        this.xtiles = this.level_width / this.tile_size;
        this.ytiles = this.level_height / this.tile_size;
        this.total_tiles = this.xtiles * this.ytiles;

        this.total_levels = 5;

        this.background = PIXI.Texture.fromImage("template_sprites/background.png");
        this.tilesheet = PIXI.Texture.fromImage("template_sprites/Tileset.png");
        this.enemysheet = PIXI.Texture.fromImage("template_sprites/CreatureSpriteSheet.png");
        this.firesheet = PIXI.Texture.fromImage("template_sprites/Fire.png");
        this.playersheet = PIXI.Texture.fromImage("template_sprites/CreatureSpriteSheet.png");
        this.tileset16sheet = PIXI.Texture.fromImage("template_sprites/tileset16.png");
    }
}

class GameTime {

    date_obj:Date;
    current_time:number;
    previous_time:number;

    constructor() {
        this.date_obj = new Date();
        this.current_time = this.date_obj.getTime() / 1000;
    }

    update() {
        this.previous_time = this.current_time;
        this.current_time = this.date_obj.getTime() / 1000;
    }

    get_time() {
        return this.current_time;
    }

    get_elapsed_time() {
        return this.current_time - this.previous_time;
    }
}

