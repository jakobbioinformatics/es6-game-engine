/// <reference path="game_utilities.ts" />
/// <reference path="game_objects.ts" />
/// <reference path="levels.ts" />

/// <reference path="light_management.ts" />

class StateManager {

    stages: Object;
    current_stage: State;

    constructor(pixi_stage:PIXI.Container, gc:GameContent) {

        this.stages = {};
        this.stages['menu'] = new MenuState(this, pixi_stage, gc);
        this.stages['level'] = new LevelInstance(this, pixi_stage, gc);
        this.stages['win'] = new WinState(this, pixi_stage, gc);

        this.current_stage = this.stages['menu'];
        this.current_stage.initialize();
    }

    change_stage(stage_key) {

        this.current_stage.deinitialize();
        this.current_stage = this.stages[stage_key];
        this.current_stage.initialize();
    }

    update(game_time) {
        game_time.update();
        this.current_stage.update(game_time);
    }
}

class State {

    pixi_stage:PIXI.Container;
    background_layer:PIXI.Container = new PIXI.Container();
    game_object_layer:PIXI.Container = new PIXI.Container();
    foreground_layer:PIXI.Container = new PIXI.Container();

    state_manager: StateManager;
    gc: GameContent;

    game_over_text: PIXI.Text;
    background_image: PIXI.Sprite;

    game_objects: GameObject[];
    obstacle_objects: GameObject[];
    background_objects: PIXI.Sprite[];
    overlay_objects: GameObject[];

    constructor(state_manager:StateManager, pixi_stage:PIXI.Container, gc:GameContent){

        this.state_manager = state_manager;
        this.pixi_stage = pixi_stage;
        this.gc = game_content;

        this.pixi_stage.addChild(this.background_layer);
        this.pixi_stage.addChild(this.game_object_layer);
        this.pixi_stage.addChild(this.foreground_layer);

        this.game_over_text = undefined;
        this.background_image = new PIXI.Sprite(game_content.background);
    }

    initialize() {

        this.game_objects = [];
        this.obstacle_objects = [];
        this.background_objects = [];
        this.overlay_objects = [];

        if (this.background_image !== undefined) {
            this.background_layer.addChild(this.background_image);
        }
    }

    deinitialize() {

        console.log('running deinitialize in state class');
        console.log(`gameobjects: ${this.game_objects.length}`);

        // Game objects
        for (var i = 0; i < this.game_objects.length; i++) {
            console.log(`Removing: ${this.game_objects[i].label}`);
            this.game_object_layer.removeChild(this.game_objects[i].get_sprite());
        }
        this.game_objects = [];

        // Obstacles
        for (var i = 0; i < this.obstacle_objects.length; i++) {
            this.game_object_layer.removeChild(this.obstacle_objects[i].get_sprite());
        }
        this.obstacle_objects = [];

        // Background objects
        for (var i = 0; i < this.background_objects.length; i++) {
            this.background_layer.removeChild(this.background_objects[i]);
        }
        this.background_objects = [];

        // Overlay objects
        for (var i = 0; i < this.overlay_objects.length; i++) {
            this.foreground_layer.removeChild(this.overlay_objects[i].get_sprite());
        }
        this.overlay_objects = [];

        if (this.background_image !== undefined) {
            this.pixi_stage.removeChild(this.background_image);
        }

        if (this.game_over_text !== undefined) {
            this.pixi_stage.removeChild(this.game_over_text);
        }
    }

    update(game_time) {

    }

    add_game_object(obj) {
        this.game_objects.push(obj);
        this.game_object_layer.addChild(obj.get_sprite());
    }

    add_obstacle_object(obj) {
        this.obstacle_objects.push(obj);
        this.game_object_layer.addChild(obj.get_sprite());
    }

    add_background_object(obj) {
        this.background_objects.push(obj);
        this.background_layer.addChild(obj);
    }

    add_overlay_object(obj) {
        this.overlay_objects.push(obj);
        this.foreground_layer.addChild(obj.get_sprite());
    }
}

class LevelState extends State {

    game_over: boolean;
    keyboard: Keyboard;
    current_level: number;
    TOT_LEVELS: number;
    player: Player;
    light_manager:LightManager;

    constructor(state_manager:StateManager, pixi_stage:PIXI.Container, gc:GameContent) {
        super(state_manager, pixi_stage, gc);

        this.light_manager = new LightManager(pixi_stage, gc);
    }

    initialize() {
        super.initialize();

        this.game_over = false;
        this.keyboard = new Keyboard();

        var start_x = 12;
        var start_y = 200;

        this._spawn_initial_setup();

        this.player = new Player(start_x, start_y, this.gc.playersheet, this.gc.level_width, this.gc.level_height);
        this.add_game_object(this.player);

        this.light_manager.assign_light_sources(this.game_objects);
        this.light_manager.generate_darkness(this);
        this.player.assign_light_sources(this.light_manager.get_light_sources());
    }

    _spawn_initial_setup() {

        var obstacle_lists = get_level_obstacles(this.current_level, this.gc);
        var obstacles = obstacle_lists[0];
        var objects = obstacle_lists[1];

        for (var i = 0; i < obstacles.length; i++) {
            this.add_obstacle_object(obstacles[i]);
        }

        for (var i = 0; i < objects.length; i++) {
            this.add_game_object(objects[i]);
        }
    }

    deinitialize() {
        console.log('deinitialize in levelstate');
        super.deinitialize();
        this.player = undefined;
    }

    update(game_time) {
        super.update(game_time);

        this.light_manager.update();

        for (var i = 0; i < this.game_objects.length; i++) {
            var obj = this.game_objects[i];
            obj.update(game_time, this.obstacle_objects, this.game_objects);
        }

        //this._remove_the_dead();

        if (this.player.is_dead() && !this.game_over) {
            this._game_over_logic();
        }

        if (this.game_over) {
            if (this.keyboard.isKeyPressed('space')) {
                this.deinitialize();
                this.initialize();
            }
        }

        if (this.player.level_win) {
            this.current_level++;
            if (this.current_level <= this.TOT_LEVELS) {
                this.deinitialize();
                this.initialize();
            }
            else {
                this.current_level = 1;
                this.state_manager.change_stage('win');
            }
        }
    }

    _game_over_logic() {
        this.game_over_text = new PIXI.Text("Game over - Space to restart", {font:"30px Courier", fill:"white"});
        this.game_over_text.position.x = 40;
        this.game_over_text.position.y = 200;
        this.pixi_stage.addChild(this.game_over_text);
        this.game_over = true;
    }

    _remove_the_dead() {
        var marked_by_death_array = [];

        for (var i = 0; i < this.game_objects.length; i++) {
            if (this.game_objects[i].is_dead()) {
                marked_by_death_array.push(this.game_objects[i]);
            }
        }

        for (var i = 0; i < marked_by_death_array.length; i++) {
            this.game_object_layer.removeChild(this.game_objects[i].get_sprite());
            this.game_objects.splice(i, 1);
        }
    }
}

class LevelInstance extends LevelState {

    current_level: number;
    TOT_LEVELS: number;

    constructor(state_manager:StateManager, stage:PIXI.Container, gc:GameContent) {
        super(state_manager, stage, gc);
        this.current_level = 1;
        this.TOT_LEVELS = 3;
    }
}

class MenuState extends State {

    start_time: number;
    delay: number;

    constructor(state_manager:StateManager, pixi_stage:PIXI.Container, gc:GameContent) {
        super(state_manager, pixi_stage, gc)
    }

    initialize() {
        super.initialize();

        var title_text = new PIXI.Text("The Hunted", {font:"50px Courier", fill:"white"});
        title_text.anchor.x = 0.5;
        title_text.position.x = 400;
        title_text.position.y = 50;
        this.add_background_object(title_text);

        var text = new PIXI.Text("Loading assets...", {font:"40px Courier", fill:"white"});
        text.anchor.x = 0.5;
        text.position.x = 400;
        text.position.y = 270;
        this.add_background_object(text);

        var helptext = new PIXI.Text("Controls: Arrows", {font:"40px Courier", fill:"white"});
        helptext.anchor.x = 0.5;
        helptext.position.x = 400;
        helptext.position.y = 170;
        this.add_background_object(helptext);

        this.start_time = 0;
        this.delay = 0.5;
    }

    update(game_time) {
        super.update(game_time);

        if (this.start_time === 0) {
            this.start_time = game_time.get_time() + this.delay;
        }
        else if (game_time.get_time() >= this.start_time) {
            //FADE_SONG.play();
            this.state_manager.change_stage('level');
        }
    }
}

class WinState extends State {

    keyboard: Keyboard;

    constructor(state_manager:StateManager, stage:PIXI.Container, gc:GameContent) {
        super(state_manager, stage, gc);
        this.keyboard = new Keyboard();
    }

    initialize() {
        super.initialize();

        var text = new PIXI.Text("You Win!!\nThanks for playing :)\n\nMade by LinkPact Games for Ludum Dare 33\n\nwww.LinkPact.com",
            {font:"30px Courier", fill:"white"});
        text.position.y = 100;
        this.add_background_object(text);
    }

    update(game_time) {
        super.update(game_time);
        if (this.keyboard.isKeyPressed('space')) {
            this.state_manager.change_stage('level');
        }
    }
}