/// <reference path="../external_libraries/pixi.js.d.ts" />
/// <reference path="movement.ts" />
/// <reference path="animation.ts" />

// ------ Classes ------ //

class GameObject {

    source_rect: PIXI.Rectangle;
    sprite: PIXI.Sprite;
    animations: Object;
    current_anim_name: string;
    current_anim: Animation;
    movement: Movement;
    is_lightsource: boolean;
    logging: boolean;
    static_only: boolean;
    alpha: number;

    max_health: number;
    current_health: number;

    player_dir: any;
    label: string;

    constructor(texture_sheet:PIXI.Texture, static_rect:PIXI.Rectangle, xpos:number, ypos:number) {

        this.source_rect = static_rect;

        var base_text = new PIXI.Texture(texture_sheet.baseTexture, new PIXI.Rectangle(static_rect.x, static_rect.y, static_rect.width, static_rect.height));
        this.sprite = new PIXI.Sprite(base_text);
        this.animations = {};
        this.current_anim_name = 'static';
        this.animations[this.current_anim_name] = new Animation(texture_sheet, static_rect);
        this.current_anim = this.animations[this.current_anim_name];

        this.set_x(xpos);
        this.set_y(ypos);

        this.alpha = 1;
        this.movement = undefined;

        this.logging = false;
        this.label = 'gameobject';
    }

    is_dead() { return this.current_health <= 0; }

    get_sprite() { return this.sprite; }
    change_animation(name:string) {

        if (name === this.current_anim_name) {
            return;
        }
        this.current_anim_name = name;
        this.current_anim = this.animations[this.current_anim_name];
    }

    get_x() { return this.sprite.position.x; }
    set_x(value) { this.sprite.position.x = value; }
    increment_x(increment) { this.sprite.position.x += increment; }

    get_y() { return this.sprite.position.y; }
    set_y(value) { this.sprite.position.y = value; }
    increment_y(increment) { this.sprite.position.y += increment; }

    width() { return this.source_rect.width; }
    height() { return this.source_rect.height; }

    get_alpha() { return this.sprite.alpha; }
    set_alpha(value) { this.sprite.alpha = value; }

    update(game_time, obstacles, game_objects) {

        if (this.movement !== undefined) {
            this.movement.update(game_time, obstacles);
        }

        if (!this.static_only) {
            this.current_anim.update(game_time);
            if (this.current_anim.has_new_frame()) {
                this.sprite.texture = this.current_anim.get_current_frame();
            }
        }
    }

    get_closest_object(objects) {

        var closest_obj = null;
        var closest_dist = 1000;
        for (var i = 0;  i < objects.length; i++) {
            var obj = objects[i];
            var distance = this.get_dist_to_object(obj);
            if (distance < closest_dist) {
                closest_dist = distance;
                closest_obj = obj;
            }
        }
        return closest_obj;
    }

    get_dist_to_object(other_obj) {
        return Math.sqrt(Math.pow(this.get_x() - other_obj.get_x(), 2) + Math.pow(this.get_y() - other_obj.get_y(), 2));
    }
}

class PlatformGameObject extends GameObject {

    constructor(spritesheet, rect, x, y) {
        super(spritesheet, rect, x, y);

        this.label = 'platformgameobject';
    }

    get_yspeed() {
        return this.movement.yspeed;
    }

    set_yspeed(value) {
        this.movement.yspeed = value;
    }

    get_is_jumping() {
        return this.movement.is_jumping;
    }

    set_is_jumping(value) {
        this.movement.is_jumping = value;
    }

    get_grounded() {
        return this.movement.grounded;
    }

    set_grounded(value) {
        this.movement.grounded = value;
    }
}

class Player extends PlatformGameObject {

    level_win: boolean;
    taking_damage: boolean;
    next_hurt_sound: number;
    light_sources: GameObject[];

    LIGHT_RADIUS: number = 100;
    LIGHT_DAMAGE: number = 1;
    REGENERATION: number = 0.16;

    constructor(x:number, y:number, player_sheet, level_width:number, level_height:number) {
        super(player_sheet, new PIXI.Rectangle(0, 0, 16, 32), x, y);
        this.movement = new PlayerPlatformMovement(this, level_width, level_height);

        this.animations['static_r'] = new Animation(player_sheet, new PIXI.Rectangle(0, 0, 16, 32));
        this.animations['static_l'] = new Animation(player_sheet, new PIXI.Rectangle(17, 0, 16, 32));
        this.animations['walk_r'] = new Animation(player_sheet, new PIXI.Rectangle(0, 32, 16, 32), 4, 0.5);
        this.animations['walk_l'] = new Animation(player_sheet, new PIXI.Rectangle(64, 32, 16, 32), 4, 0.5);
        this.animations['jump_r'] = new Animation(player_sheet, new PIXI.Rectangle(0, 64, 16, 30));
        this.animations['jump_l'] = new Animation(player_sheet, new PIXI.Rectangle(16, 64, 16, 30));
        this.animations['hurt_r'] = new Animation(player_sheet, new PIXI.Rectangle(0, 94, 16, 32));
        this.animations['hurt_l'] = new Animation(player_sheet, new PIXI.Rectangle(16, 94, 16, 32));

        this.max_health = 80;
        this.current_health = this.max_health;

        this.light_sources = [];
        this.level_win = false;
        this.taking_damage = false;
        this.next_hurt_sound = 0;
        this.is_lightsource = false;

        this.label = 'player';
    }

    update(game_time, obstacles, game_objects) {
        super.update(game_time, obstacles, game_objects);

        for (var i = 0; i < game_objects.length; i++) {
            var obj = game_objects[i];

            if (obj === this) {
                continue;
            }

            if (intersectObjects(this, obj)) {
                if (obj instanceof GoalSquare) {
                    this.level_win = true;
                    //COMPLETE_SOUND.play();
                    //this.state_manager.change_stage('win');
                }
            }
        }

        this._update_light_damage();

        if (this.current_health < this.max_health && !this.taking_damage) {
            this.current_health += this.REGENERATION;
        }

        if (this.taking_damage && game_time.get_time() > this.next_hurt_sound) {
            //HURT_SOUND.play();
            this.next_hurt_sound = game_time.get_time() + 0.5;
        }
        else {
            //HURT_SOUND.stop();
        }

        this._update_anim_choice();
        this.alpha = this.current_health / this.max_health;
    }

    assign_light_sources(light_sources:GameObject[]) {
        this.light_sources = light_sources;
    }

    _update_light_damage() {

        this.taking_damage = false;
        if (this.light_sources.length > 0) {

            var closest = this.get_closest_object(this.light_sources);
            var closest_dist = this.get_dist_to_object(closest);
            if (closest_dist < this.LIGHT_RADIUS) {
                this.current_health -= this.LIGHT_DAMAGE;
                this.taking_damage = true;
            }
        }
    }

    _update_anim_choice() {
        if (this.taking_damage) {
            if (this.movement.get_xspeed() > 0) {
                this.change_animation('hurt_r');
            }
            else {
                this.change_animation('hurt_l');
            }
        }
        else if (this.movement.is_jumping) {
            if (this.movement.get_xspeed() > 0) {
                this.change_animation('jump_r');
            }
            else {
                this.change_animation('jump_l');
            }
        }
        else if (Math.abs(this.movement.get_xspeed()) > 0.1) {
            if (this.movement.get_xspeed() > 0) {
                this.change_animation('walk_r');
            }
            else {
                this.change_animation('walk_l');
            }
        }
        else {
            if (this.movement.get_xspeed() >= 0) {
                this.change_animation('static_r');
            }
            else {
                this.change_animation('static_l');
            }
        }
    }
}

class EnemyCharacter extends PlatformGameObject {

    movement: EnemyPlatformMovement;
    SIGHT_RANGE: number;
    STOP_RANGE: number;

    constructor(x:number, y:number, ENEMY_SHEET:PIXI.Texture, level_width:number, level_height:number) {
        var yoff = -7;
        super(ENEMY_SHEET, new PIXI.Rectangle(0, 0, 16, 23), x, y+yoff);
        this.animations['static_r'] = new Animation(ENEMY_SHEET, new PIXI.Rectangle(0, 0, 16, 23), 2, 1);
        this.animations['static_l'] = new Animation(ENEMY_SHEET, new PIXI.Rectangle(32, 0, 16, 23), 2, 1);
        this.animations['walk_r'] = new Animation(ENEMY_SHEET, new PIXI.Rectangle(0, 23, 16, 23), 4, 1);
        this.animations['walk_l'] = new Animation(ENEMY_SHEET, new PIXI.Rectangle(64, 23, 16, 23), 4, 1);

        this.movement = new EnemyPlatformMovement(this, level_width, level_height);

        this.change_animation('static_l');
        this.is_lightsource = true;

        this.SIGHT_RANGE = 150;
        this.STOP_RANGE = 20;
        this.player_dir = Direction.NONE;

        this.label = 'enemycharacter';
    }

    update(game_time, obstacles, game_objects) {
        super.update(game_time, obstacles, game_objects);

        var player_ref;
        for (var i = 0; i < game_objects.length; i++) {
            var obj = game_objects[i];
            if (obj instanceof Player) {
                player_ref = obj;
                break;
            }
        }

        if (player_ref) {
            this.player_dir = this._get_player_dir(player_ref);
        }

        this._update_anim_choice();
    }

    _update_anim_choice() {
        if (Math.abs(this.movement.xspeed) > 0.1) {
            if (this.movement.xspeed > 0) {
                this.change_animation('walk_r');
            }
            else {
                this.change_animation('walk_l');
            }
        }
        else {
            if (this.movement.xspeed >= 0) {
                this.change_animation('static_r');
            }
            else {
                this.change_animation('static_l');
            }
        }
    }

    _get_player_dir(player_ref) {

        var dist = this.get_dist_to_object(player_ref);
        if (dist < this.SIGHT_RANGE && dist > this.STOP_RANGE) {
            if (this.get_x() < player_ref.x) {
                return Direction.RIGHT;
            }
            else {
                return Direction.LEFT;
            }
        }
        else {
            return Direction.NONE;
        }
    }
}

class StaticSquare extends GameObject {

    constructor(x, y, tile_nbr, gc) {

        var tile_size = gc.tile_size;
        var tile_sheet = gc.tilesheet;

        tile_nbr -= 1;

        var xpos = tile_nbr % 3 * tile_size;
        var ypos = Math.floor(tile_nbr / 3) * tile_size;

        var rect = new PIXI.Rectangle(xpos, ypos, tile_size, tile_size);
        super(tile_sheet, rect, x, y);

        this.label = 'staticsquare';
    }
}

class GoalSquare extends GameObject {

    constructor(x, y, PORTAL_SHEET) {
        super(PORTAL_SHEET, new PIXI.Rectangle(0, 0, 16, 32), x, y-16);
        this.label = 'goalsquare';
    }
}

class FireSquare extends GameObject {

    constructor(x, y, TILE_SIZE, FIRE_SHEET) {
        super(FIRE_SHEET, new PIXI.Rectangle(0, 0, TILE_SIZE, TILE_SIZE), x, y);
        this.is_lightsource = true;
        this.animations['fire'] = new Animation(FIRE_SHEET, new PIXI.Rectangle(0, 0, TILE_SIZE, TILE_SIZE), 3, 1);
        this.current_anim = this.animations['fire'];
        this.label = 'firesquare';
    }
}