/// <reference path="game_objects.ts" />
/// <reference path="keyboard.ts" />
/// <reference path="collision_handling.ts" />

var Direction = {
    'LEFT': 1,
    'UP': 2,
    'RIGHT': 3,
    'DOWN': 4,
    'NONE': 0
};

var EdgeBehaviour = {
    'NONE': 1,
    'STOP': 2,
    'STOP_PLATFORM': 3,
    'BOUNCE': 4
};


class Movement {

    LEVEL_WIDTH: number;
    LEVEL_HEIGHT: number;

    movement_object: GameObject;
    speed: number;

    xspeed: number;
    get_xspeed() { return this.xspeed; }

    yspeed: number;
    get_yspeed() { return this.yspeed; }

    edge_behaviour: Object;
    grounded: boolean;
    is_jumping: boolean;

    constructor(movement_object, level_width, level_height) {

        this.LEVEL_WIDTH = level_width;
        this.LEVEL_HEIGHT = level_height;

        this.movement_object = movement_object;
        this.xspeed = 0;
        this.yspeed = 0;
        this.edge_behaviour = EdgeBehaviour.NONE;
    }

    update(game_time, obstacles) {
        this.movement_object.set_x(this.movement_object.get_x() + this.xspeed);

        if (!this.grounded) {
            this.movement_object.set_y(this.movement_object.get_y() + this.yspeed);
        }

        if (this.edge_behaviour !== EdgeBehaviour.NONE) {
            this._handle_edge_behaviour();
        }
    }

    _handle_edge_behaviour() {
        var edge_collisions = this.get_edge_collisions();
        if (edge_collisions.length > 0) {
            if (this.edge_behaviour == EdgeBehaviour.BOUNCE) {
                this._bounce_at_edges(edge_collisions);
            }
            else if (this.edge_behaviour == EdgeBehaviour.STOP
                || this.edge_behaviour == EdgeBehaviour.STOP_PLATFORM) {
                this._stop_at_edges(this.edge_behaviour, edge_collisions);
            }
            else {
                throw new Error("Unexpected edge behaviour: " + this.edge_behaviour);
            }
        }
    }

    _bounce_at_edges(edges) {
        for (var i = 0; i < edges.length; i++) {
            switch (edges[i]) {
                case Direction.LEFT: {
                    this.xspeed = Math.abs(this.xspeed);
                    break;
                }
                case Direction.UP: {
                    this.yspeed = Math.abs(this.yspeed);
                    break;
                }
                case Direction.RIGHT: {
                    this.xspeed = -Math.abs(this.xspeed);
                    break;
                }
                case Direction.DOWN: {
                    this.yspeed = -Math.abs(this.yspeed);
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }

    _stop_at_edges(behaviour, edges) {
        for (var i = 0; i < edges.length; i++) {
            switch (edges[i]) {
                case Direction.LEFT: {
                    this.movement_object.set_x(0);
                    break;
                }
                case Direction.UP: {
                    this.movement_object.set_y(0);
                    if (behaviour === EdgeBehaviour.STOP_PLATFORM) {
                        this.yspeed = 0;
                    }
                    break;
                }
                case Direction.RIGHT: {
                    this.movement_object.set_x(this.LEVEL_WIDTH - this.movement_object.width());
                    break;
                }
                case Direction.DOWN: {
                    this.movement_object.set_y(this.LEVEL_HEIGHT - this.movement_object.height());
                    if (behaviour === EdgeBehaviour.STOP_PLATFORM) {
                        this.is_jumping = false;
                        this.grounded = true;
                        this.yspeed = 0;
                    }
                    break;
                }
                default: {
                    break;
                }
            }
        }
    }

    get_edge_collisions() {

        var edge_collisions = [];

        if (this.movement_object.get_x() < 0) {
            edge_collisions.push(Direction.LEFT);
        }
        else if (this.movement_object.get_x() + this.movement_object.width() > this.LEVEL_WIDTH) {
            edge_collisions.push(Direction.RIGHT);
        }

        if (this.movement_object.get_y() < 0) {
            edge_collisions.push(Direction.UP);
        }
        if (this.movement_object.get_y() + this.movement_object.height() > this.LEVEL_HEIGHT) {
            edge_collisions.push(Direction.DOWN);
        }

        return edge_collisions;
    }
}

class RandomBounceMovement extends Movement {

    constructor(game_object: GameObject, level_width: number, level_height: number) {
        super(game_object, level_width, level_height);

        this.xspeed = 2 * Math.random() * this.speed - this.speed;
        this.yspeed = 2 * Math.random() * this.speed - this.speed;

        this.edge_behaviour = EdgeBehaviour.BOUNCE;
    }

    update(game_time, obstacles) {
        super.update(game_time, obstacles);
    }
}

class PlatformMovement extends Movement {

    GRAVITY: number;
    FRICTION: number;
    MAX_SPEED: number;
    JUMPSTRENGTH: number;

    constructor(game_object:GameObject, level_width:number, level_height:number) {
        super(game_object, level_width, level_height);

        this.GRAVITY = 0.3;
        this.FRICTION = 0.8;
        this.MAX_SPEED = 1.5;
        this.JUMPSTRENGTH = 0;

        this.edge_behaviour = EdgeBehaviour.STOP_PLATFORM;
        this.is_jumping = false;
        this.grounded = false;
    }

    update(game_time, obstacles) {
        super.update(game_time, obstacles);

        this.xspeed *= this.FRICTION;
        this.yspeed += this.GRAVITY;

        this._update_obstacle_collision(obstacles);
    }

    _update_obstacle_collision(obstacles) {

        this.grounded = false;
        for (var i = 0; i < obstacles.length; i++) {

            var obj = obstacles[i];
            var dir = correctingColCheck(this.movement_object, obj);

            if (dir === Direction.LEFT || dir === Direction.RIGHT ||
                dir === Direction.UP || dir === Direction.DOWN) {
            }

            if (dir === Direction.LEFT || dir === Direction.RIGHT) {
                this.is_jumping = false;
            }
            else if (dir === Direction.DOWN) {
                this.is_jumping = false;
                this.grounded = true;
                this.yspeed = 0;
            }
            else if (dir === Direction.UP) {
                this.yspeed = 0;
            }
        }
    }
}

class PlayerPlatformMovement extends PlatformMovement {

    keyboard: Keyboard;

    constructor(game_object, level_width, level_height) {
        super(game_object, level_width, level_height);

        this.keyboard = new Keyboard();
        this.FRICTION = 0.8;
        this.MAX_SPEED = 1.8;
        this.JUMPSTRENGTH = 7.5;
    }

    update(game_time, obstacles) {
        super.update(game_time, obstacles);
        this._update_keyboard_controls();
    }

    _update_keyboard_controls() {

        if (this.keyboard.isKeyPressed('left')) {
            this.xspeed = -this.MAX_SPEED;
        }
        else if (this.keyboard.isKeyPressed('right')) {
            this.xspeed = this.MAX_SPEED;
        }

        if (this.keyboard.isKeyPressed('up') && !this.is_jumping && this.grounded) {
            this._jump();
        }
    }

    _jump() {
        this.is_jumping = true;
        this.grounded = false;
        this.yspeed = - this.JUMPSTRENGTH;
    }
}

class EnemyPlatformMovement extends PlatformMovement {

    keyboard: Keyboard;

    constructor(game_object:GameObject, level_width:number, level_height:number) {
        super(game_object, level_width, level_height);

        this.keyboard = new Keyboard();
        this.FRICTION = 0.8;
        this.MAX_SPEED = 0.5;
        this.JUMPSTRENGTH = 7;
    }

    update(game_time, obstacles) {
        super.update(game_time, obstacles);

        if (this.movement_object.player_dir === Direction.LEFT) {
            this.xspeed = -this.MAX_SPEED;
        }
        else if (this.movement_object.player_dir === Direction.RIGHT) {
            this.xspeed = this.MAX_SPEED;
        }
    }
}

class PlayerTopDownMovement extends Movement {

    keyboard: Keyboard;

    constructor (game_object: GameObject, level_width: number, level_height: number) {
        super(game_object, level_width, level_height);

        this.keyboard = new Keyboard();
    }

    update(game_time, obstacles) {
        super.update(game_time, obstacles);
        this._update_keyboard_controls();
    }

    _update_keyboard_controls() {

        var hasMoved = false;

        if (this.keyboard.isKeyPressed('left')) {
            hasMoved = true;
        }
        else if (this.keyboard.isKeyPressed('right')) {
            hasMoved = true;
        }

        if (this.keyboard.isKeyPressed('up')) {
            hasMoved = true;
        }
        else if (this.keyboard.isKeyPressed('down')) {
            hasMoved = true;
        }

        if (hasMoved) {
            this.speed = 5;
        }
        else {
            this.speed = 0;
        }
    }
}