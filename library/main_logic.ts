/// <reference path="game_utilities.ts" />
/// <reference path="game_objects.ts" />
/// <reference path="states.ts" />
/// <reference path="levels.ts" />

var pixi_stage = new PIXI.Container();
//var overlay_container = new PIXI.Container();

var renderer;
var game_content = new GameContent();
var state_manager = new StateManager(pixi_stage, game_content);

function init() {

    renderer = PIXI.autoDetectRenderer(game_content.level_width, game_content.level_height);
    document.body.appendChild(renderer.view);
    renderer.backgroundColor = 0x220066;
    requestAnimationFrame(update);
}

function update() {

    var game_time = new GameTime();
    renderer.render(pixi_stage);
    requestAnimationFrame(update);
    state_manager.update(game_time);
}

